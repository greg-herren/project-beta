from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import (
    AutomobileVODetailEncoder,
    SalespersonListEncoder,
    CustomerListEncoder,
    SaleListEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the salespeople"},
                status=400,
            )


@require_http_methods(["DELETE"])
def api_show_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(employee_id=pk).delete()
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )
    except Salesperson.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the customer"},
                status=400,
            )


@require_http_methods(["DELETE"])
def api_show_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk).delete()
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )
    except Customer.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            # vin
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile

            # salesperson
            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = salesperson

            # customer
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_show_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk).delete()
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
    except Sale.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
