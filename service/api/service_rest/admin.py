from django.contrib import admin

# Register your models here.
from .models import Technician, AutomobileVO, Appointment


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = ["id", "first_name", "last_name", "employee_id"]


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ["id", "vin", "import_href", "sold"]


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ["id", "isVip", "date_time", "reason", "status", "vin", "customer", "technician"]
