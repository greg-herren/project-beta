from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Technician, AutomobileVO, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_technicians(request):
    """
    Collection RESTful API handler for Technician objects in
    the service center.

    GET:
    Returns a dictionary with a single key "technicians" which
    is a list of the first name, last name, and employee ID of
    the technician.

    {
        "technicians": [
            {
                "first_name": first name of the technician,
                "last_name": first name of the technician,
                "employee_id": the employee ID of the technician,
            },
            ...
        ]
    }

    POST:
    Creates a technician resource and returns its details.
    {
        "first_name": first name of the technician,
        "last_name": first name of the technician,
        "employee_id": the employee ID of the technician,
    }
    """
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    try:
        technician = Technician.objects.get(employee_id=id)
        technician.delete()
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Technician does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    """
    Collection RESTful API handler for Appointment objects in
    the service center.

    GET:
    Returns a dictionary with a single key "appointments" which
    is a list of the date_time, reason, status, vin, customer, and
    technician of the appointment.

    {
        "appointments": [
            {
                "date_time": the date and time of the appointment,
                "reason": the reason for the appointment,
                "status": the status of the appointment,
                "vin": the vin of the car for the appointment
                "customer": the name of the customer for the appointment
                "technician": the name of the technician for the appointment
            },
            ...
        ]
    }

    POST:
    Creates an appointment and returns its details.
    {
        "date_time": the date and time of the appointment,
        "reason": the reason for the appointment,
        "status": the status of the appointment,
        "vin": the vin of the car for the appointment
        "customer": the name of the customer for the appointment
        "technician": the name of the technician for the appointment
    }
    """
    if request.method == "GET":
        appointments = Appointment.objects.all()
        for appointment in appointments:
            if len(AutomobileVO.objects.filter(vin=appointment.vin)) > 0:
                appointment.isVip = True
                appointment.save()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            try:
                technician_employee_id = content["technician"]
                technician = Technician.objects.get(employee_id=technician_employee_id)
                content["technician"] = technician
            except Technician.DoesNotExist:
                response = JsonResponse({"message": "Invalid technician"})
                response.status_code = 400
                return response
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the appointment"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_update_appointment(request, id, status):
    if status in ["cancel", "finish"]:
        try:
            appointment = Appointment.objects.get(id=id)
            if status == "cancel":
                appointment.status = "canceled"
            elif status == "finish":
                appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        response = JsonResponse({"message": "Invalid status"})
        response.status_code = 400
        return response
