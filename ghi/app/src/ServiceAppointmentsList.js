import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";


function formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDay()
    return `${month}/${day}/${year}`
}


function formatTime(dateString) {
    let date = new Date(dateString);
    let hours = date.getUTCHours();
    let minutes = ("00" + date.getUTCMinutes()).slice(-2);
    let AMPM = 'AM';
    if (hours > 12) {
        hours -= 12;
        AMPM = 'PM';
    }
    return `${hours}:${minutes} ${AMPM}`
}


function ServiceAppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    const fetchAppointmentData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            let active_appointments = []
            for (let appointment of data.appointments) {
                if (appointment.status == "scheduled") {
                    active_appointments.push(appointment)
                }
            }
            setAppointments(active_appointments);
        }
    };

    useEffect(() => {
        fetchAppointmentData();
    }, []);

    async function handleStatusUpdate(e, index) {
        const action = e.target.name;
        const id = e.target.value;
        const url = `http://localhost:8080/api/appointments/${id}/${action}`;
        const fetchConfig = {method: "put"};
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchAppointmentData();
        }
    }

    return (
        <>
            <h1 className="mt-4">Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.isVip ? "Yes" : "No" }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ formatDate(appointment.date_time) }</td>
                                <td>{ formatTime(appointment.date_time) }</td>
                                <td>{ `${appointment.technician.first_name} ${appointment.technician.last_name}` }</td>
                                <td>{ appointment.reason }</td>
                                <td>
                                    <button onClick={(e)=> handleStatusUpdate(e,idx)} value={appointment.id} name="cancel" type="button" className="btn btn-danger m-1">
                                        Cancel
                                    </button>
                                    <button onClick={(e)=> handleStatusUpdate(e,idx)} value={appointment.id} name="finish" type="button" className="btn btn-success m-1">
                                        Finish
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to="/appointments/create">
                <button type="button" className="btn btn-primary">
                    Create a service appointment
                </button>
            </Link>
        </>
    );
}

export default ServiceAppointmentsList;
