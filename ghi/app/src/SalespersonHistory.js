import React, { useState, useEffect } from 'react';


function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesUpdate, setSalesUpdate] = useState([]);

    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data2 = await response.json();
            setSalespeople(data2.salespeople);
        }
    };

    const fetchData2 = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if(response.ok) {
            const data = await response.json();
            setSales(data.sales);
            setSalesUpdate(data.sales);
        }
    }

    useEffect(() => {
        fetchData();
        fetchData2();
    }, []);

    const handleChange = (event) => {
        var filteredSalespeople;
        const value = event.target.value;
        if (value === "") {
            filteredSalespeople = sales
        } else {
            filteredSalespeople = sales.filter(sale => sale.salesperson.employee_id === parseFloat(value))
        }
        setSalesUpdate(filteredSalespeople);
    }

    return (
        <div>
            <h1 className="mt-4">Salesperson History</h1>
            <div className="mb-3">
                <label htmlFor="salesperson">Choose a Salesperson</label>
                <select onChange={handleChange} required id="salesperson" name="salesperson" className="form-select">
                    <option value="">All Salespeople</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {salesUpdate.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                        );
                })}
            </tbody>
            </table>
        </div>

    );
}

export default SalespersonHistory;
