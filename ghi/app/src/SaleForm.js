import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";


function SaleForm() {
    const navigate = useNavigate()

    const [autos, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [formData, setFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });

    }

    const handleSalesSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const vin = formData.automobile;
            const soldUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const soldFetchConfig = {
                method:"PUT",
                body: JSON.stringify({"sold": true}),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const soldResponse = await fetch(soldUrl, soldFetchConfig);
            if (soldResponse.ok) {
                alert("Succesfully created")
                setFormData({
                    automobile: "",
                    salesperson: "",
                    customer: "",
                    price: "",
                })
                navigate("/sales");
            }

        }

    }

    const fetchData = async () => {

        const response1 = await fetch("http://localhost:8100/api/automobiles/");
        const response2 = await fetch("http://localhost:8090/api/salespeople/");
        const response3 = await fetch("http://localhost:8090/api/customers/");

        if (response1.ok && response2.ok && response3.ok) {
            const data1 = await response1.json();
            const data2 = await response2.json();
            const data3 = await response3.json();
            setAutomobiles(data1.autos);
            setSalespeople(data2.salespeople)
            setCustomers(data3.customers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const notSold = autos.filter(auto => auto.sold === false);

    return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={handleSalesSubmit} id="create-sale-form">
                <div className="mb-3">
                    <label htmlFor="automobile">Automobile VIN</label>
                    <select value={formData.automobile} onChange={handleFormChange} required id="automobile" name="automobile" className="form-select">
                        <option value="">Choose an automobile VIN</option>
                        {notSold.map(automobile => {
                            return (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="salesperson">Salesperson</label>
                    <select value={formData.salesperson} onChange={handleFormChange} required id="salesperson" name="salesperson" className="form-select">
                        <option value="">Choose a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="customer">Customer</label>
                    <select value={formData.customer} onChange={handleFormChange} required id="customer" name="customer" className="form-select">
                        <option value="">Choose a customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="price">Price</label>
                    <input value={formData.price} onChange={handleFormChange} placeholder="0" required type="number" name="price" id="price" className="form-control" />
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default SaleForm;
