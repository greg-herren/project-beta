import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import Nav from './Nav';
import ModelForm from './ModelForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import SalespersonForm from './SalespersonForm';
import SalespeoplesList from './SalespeopleList';
import CustomersList from './CustomersList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentsList from './ServiceAppointmentsList';
import ServiceAppointmentsForm from './ServiceAppointmentsForm';
import ServiceHistory from './ServiceHistory';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/models/create" element={<ModelForm/>} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/create" element={<AutomobileForm/>} />
          <Route path="salespeople" element={<SalespeoplesList />} />
          <Route path="salespeople/create" element={<SalespersonForm/>} />
          <Route path="/customers" element={<CustomersList />} />
          <Route path="/customers/create" element={<CustomerForm/>} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/create" element={<SaleForm/>} />
          <Route path="/sales/history" element={<SalespersonHistory />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="technicians/create" element={<TechnicianForm />} />
          <Route path="appointments" element={<ServiceAppointmentsList />} />
          <Route path="appointments/create" element={<ServiceAppointmentsForm />} />
          <Route path="appointments/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
