import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";


function ModelList() {
    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1 className="mt-4">Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map((model, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>
                                    <img src={ model.picture_url } width="200px"/>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to="/models/create">
                <button type="button" className="btn btn-primary">
                    Add new model
                </button>
            </Link>
        </>
    );
}

export default ModelList;
